
import Realm
import RealmSwift

enum State: String {
    case train = "train"
    case track = "track"
    case unknown = "unknown"
}

class Interval: Object {
    @objc dynamic var beginTime: Date = Date()
    @objc dynamic var state: String? = "train"

    init(beginTime: Date, state: State) {
        super.init()
        
        self.beginTime = beginTime.roundMinute()
        self.state = state.rawValue
    }
    
    // MARK: - Realm required initializers
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    // MARK: - Save interval to DB
    @discardableResult
    func save() -> Bool {
        
        if DB.writeToDB(newInterval: self) {
            return true
        }
        
        return false
    }
}

extension Array where Element: Interval {
    func filterUniqueElements() -> [Interval] {
        var filtered = [Interval]()
        
        let result = self.filter { (interval) -> Bool in
            guard filtered.index(where: { $0.beginTime == interval.beginTime }) == nil
                else { return false }
            filtered.append(interval)
            return true
        }
        return result
    }
}

// MARK: - Date extension
extension Date {
    
    func roundMinute() -> Date {
        let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        let cal = Calendar.current
        var components = cal.dateComponents(x, from: self)
        
        components.second = 0
        
        return cal.date(from: components)!
    }
}

