
import RealmSwift

let DB = DBModule.sharedInstance

class DBModule {
    static let sharedInstance = DBModule()
    
    fileprivate let realm = try! Realm()
    fileprivate let intervals = try! Realm().objects(Interval.self).sorted(byKeyPath: "beginTime", ascending: true)
}

// MARK: Write data
extension DBModule {
    
    // Save an interval to DB
    @discardableResult
    func writeToDB(newInterval: Interval) -> Bool {
        let result = intervals.filter("beginTime == %@", newInterval.beginTime)
        if result.isEmpty {
            try! self.realm.write {
                self.realm.add(newInterval)
            }
            return true
        }
        return false
    }

    @discardableResult
    func writeToDB(intervals: [Interval]) -> Bool {
        let realm = try! Realm()
        
        let filteredIntervals = intervals.filterUniqueElements()
        let allIntervals = realm.objects(Interval.self)
        
        let result = filteredIntervals.filter { (interval) -> Bool in
            let duplicateIndex = allIntervals.index(where: { $0.beginTime == interval.beginTime })
            let result = duplicateIndex == nil
            
            if result {
                try! realm.write {
                    realm.add(interval)
                }
            }
            
            return result
        }
        
        return result.count == intervals.count
    }
}

// MARK: Get data
extension DBModule {
    // Return array of days (start of the day) from all intervals
    func getArrayOfTrainingDays(completion: @escaping ([Date]) -> Void) {
        DispatchQueue.global(qos: .background).async {
            let intervals = try! Realm().objects(Interval.self).sorted(byKeyPath: "beginTime", ascending: true)
            let allTrainingDates = (intervals.value(forKey: "beginTime") as! [Date]).map { NSCalendar.current.startOfDay(for: $0) }
            completion(Array(Set(allTrainingDates)).sorted())
        }
    }
    
    // Return all intervals of today
    func getTodayTrainingIntervals(completion: @escaping (Results<Interval>) -> Void) {
        getIntervalsInDate(Date(), withStateFilter: .train, completion: completion)
    }
    
    // Return all intervals filtered by day and state
    func getIntervalsInDate(_ date: Date, withStateFilter state: State,
                            completion: @escaping (Results<Interval>) -> Void) {
        getIntervalsInDate(date) { (result) in
            let filetred = result.filter("state == %@", state.rawValue)
            completion(filetred)
        }
    }
}

// MARK: private functions
private extension DBModule {
    func getIntervalsInDate(_ date: Date, completion: @escaping (Results<Interval>) -> Void) {
        let dayStart = NSCalendar.current.startOfDay(for: date)
        let dayEnd: Date = {
            var components = DateComponents()
            components.day = 1
            components.second = -1
            return NSCalendar.current.date(byAdding: components, to: dayStart)!
        }()
        
        DispatchQueue.global(qos: .background).async {
            let intervals = try! Realm().objects(Interval.self).sorted(byKeyPath: "beginTime", ascending: true)
            let result = intervals.filter("beginTime BETWEEN %@", [dayStart, dayEnd])
            completion(result)
        }
    }
}
