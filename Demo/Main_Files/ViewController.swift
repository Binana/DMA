
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        updateDatabase()
        updateStatsView()
    }
}

private extension ViewController {
    
    func updateDatabase() {
        let date = Date()
        for i in 1 ... 1000 {
            if let date = NSCalendar.current.date(byAdding: .second, value: -i, to: date) {
                let interval = Interval(beginTime: date, state: .train)
                interval.save()
            }
        }
    }
    
    func updateStatsView() {
        
        // Print all the intervals dates
        DB.getArrayOfTrainingDays { (arrayOfTrainingDays) in
            self.print(items: arrayOfTrainingDays)
        }
        
        // Print first training interval begin time from today

        DB.getTodayTrainingIntervals { (todayTrainingIntervals) in
            guard let todayTrainingInterval = todayTrainingIntervals.first else { return }
            self.print(items: todayTrainingInterval.beginTime)
        }

        // Print first training and tracking intervals begin time from yesterday
        if let yesterday = NSCalendar.current.date(byAdding: .day, value: -1, to: Date()) {
            DB.getIntervalsInDate(yesterday, withStateFilter: .train, completion: { (result) in
                let trainIntervals = result.sorted(byKeyPath: "beginTime", ascending: true)
                guard let trainInterval = trainIntervals.first else { return }
                self.print(items: trainInterval.beginTime)
            })
            
            DB.getIntervalsInDate(yesterday, withStateFilter: .track, completion: { (result) in
                let trackIntervals = result.sorted(byKeyPath: "beginTime", ascending: true)
                guard let trackInterval = trackIntervals.first else { return }
                self.print(items: trackInterval.beginTime)
            })
        }
    }
}

private extension ViewController {
    func print(items: Any..., thread: DispatchQueue = .main) {
        thread.async {
            Swift.print(items.first ?? "No items to print")
        }
    }
    
    // oh, and btw
    // difference between sync and async
    // is that thread that called sync operation,
    // will wait for the sync block to finish
    // before continuing
}
